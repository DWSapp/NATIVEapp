# Projet d'application mobile native avec AngularJS et NAtiveScript
Pour utiliser ce repo il faut configurer votre espace de travail en suivant les instructions disponible à l'adresse suivante :
[http://docs.nativescript.org/angular/start/quick-setup](http://docs.nativescript.org/angular/start/quick-setup)

## Installation de dépendance (MAC)
```bash
-> Installer NativeScript CLI
npm install -g nativescript

-> Installer les extensions requises pour IOS et Android
ruby -e "$(curl -fsSL https://www.nativescript.org/setup/mac)"
}
```

## Liens
Lien frameworks : [https://www.nativescript.org/](https://www.nativescript.org/)

Lien configuration : [http://docs.nativescript.org/angular/tutorial/ng-chapter-1](http://docs.nativescript.org/angular/tutorial/ng-chapter-1)

Lien templates : [http://nativescript.rocks/templates.php](http://nativescript.rocks/templates.php)

Lien template hello world -ng : [https://github.com/NativeScript/template-hello-world-ts](https://github.com/NativeScript/template-hello-world-ts)