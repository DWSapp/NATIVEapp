import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";

import { CategoryList } from "../shared/category/category";


@Injectable()
export class WordpressService {
  constructor(private http: Http) {}

  // Création d'une fonction pour charger la liste des catégories Memo de l'API WP
  getCategoryList (): Promise<any[]> {
    return this.http.get("https://juliennoyer.com/wp-json/wp/v2/categories?order=asc&orderby=id")
    .toPromise().then(this.getDataFromWordpress).catch(this.handleError);
  }

  // Création d'une fonction pour charge les posts d'une catégorie de l'API WP
  getMemoList (slug: any): Promise<any[]> {
    return this.http.get("https://juliennoyer.com/wp-json/wp/v2/posts?order=asc&orderby=id&filter[category_name]=" + slug)
    .toPromise().then(this.getDataFromWordpress).catch(this.handleError);
  }

  // Traitement de la réponse json
  private getDataFromWordpress(res: Response) {
    return res.json() || { };
  }

  // Traitement des erreurs
  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Promise.reject(errMsg);
  }
}