import {Component, OnInit} from "@angular/core";
import { Router } from '@angular/router';

import { CategoryList } from "../../shared/category/category";
import { WordpressService } from "../../services/wordpress.serv";


@Component({
  selector: "dashboard-cmp",
  templateUrl: "components/dashboard/dashboard.html",
  providers: [ WordpressService ]
})

export class DashboardComponent implements OnInit {

  
  
  public collection:Array<CategoryList>;
  private errorMessage: string; 

  constructor(private wordpressService: WordpressService, private router: Router) { }

  getMemosFromWordpress() {
    this.wordpressService.getCategoryList()
    .then( data => this.collection = data, error =>  this.errorMessage = <any>error);
  }

  // Création d'un fonction pour définir la route d'un item
  getWpMemoItem(memoItem: any): void {
    alert(memoItem)
    // this.router.navigate(memoName); 
  };

  getPage(slug){
    let memoName = ['/wordpress/'+ slug];
    // alert(memoName)
    this.router.navigate(memoName);
  }

  ngOnInit() { 
    this.getMemosFromWordpress();
  }

}