import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { WordpressService } from "../../services/wordpress.serv";

var utilityModule = require("utils/utils");


@Component({
  selector: "memo-cmp",
  templateUrl: "components/memo/memo.html",
  providers: [ WordpressService ]
})

export class MemoComponent implements OnInit {
  
  public collectionTitle:string;
  public collection:any;
  private errorMessage: string; 

  constructor (
        // Ajout des services dans le constructor
        private wordpressService: WordpressService,
        private route: ActivatedRoute,
    ) {}

  getMemoContent(slug :any){
    this.wordpressService.getMemoList(slug).then( data => this.collection = data, error =>  this.errorMessage = <any>error)
  }
  

  getUrl(url:any){
    utilityModule.openUrl(url);
  }

  ngOnInit() {
    // Récupération de la route et appel de la fonction pour afficher la liste de memo
    this.route.params.forEach((params: Params) => {
      let slug = params['memoName'];
      this.getMemoContent(slug);
      console.log(slug);
      this.collectionTitle = slug;
    }); 
  }

}