import {Component, OnInit} from "@angular/core";
import { getFile, getImage, getJSON, getString, request } from "http";
import * as layout from "ui/layouts/wrap-layout"

import * as pageModule from "ui/page"


@Component({
  selector: "dashboard-cmp",
  templateUrl: "components/category/category.html"
})
export class CategoryComponent implements OnInit {
  
  public collection:Array<Object>; 

  getCategories(){

        fetch("https://juliennoyer.com/wp-json/wp/v2/categories", { method: 'get' })
        .then(response => { return response.json() })

        .then(data => {
            this.collection = data;
            for(let i = 0; i < data.length; i++){
                console.log(data[i].name)
            }

        }).catch(err => {
            // Error 
        });  
    };

    getPage(slug){
        alert(slug)
    }

    ngOnInit() { 
        this.getCategories();
    }


}