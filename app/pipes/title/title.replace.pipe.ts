import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'titleReplace'})
export class TitleReplacePipe implements PipeTransform {
  transform(value: string, args: string[]): any {
    if (!value) return value;

    return value.replace(/\-/g, ' ')
  }
}