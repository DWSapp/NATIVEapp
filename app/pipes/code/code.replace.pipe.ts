import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'codeReplace'})
export class CodeReplacePipe implements PipeTransform {
  transform(value: string, args: string[]): any {
    if (!value) return value;

    return value.replace(/\&lt;/g, '<').replace(/\&gt;/g, '>').replace(/\&#039;/g, "'").replace(/\&quot;/g, '')
  }
}