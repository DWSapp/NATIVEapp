import { NgModule } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptModule } from "nativescript-angular/platform";
import { NativeScriptRouterModule } from "nativescript-angular/router";


import { AppComponent } from "./app.component";

import { routes, navigatableComponents } from "./app.routing";

import { DashboardComponent } from "./components/dashboard/dashboard.cmp";
import { CategoryComponent } from "./components/category/category.cmp";
import { MemoComponent } from "./components/memo/memo.cmp";
import { CodeReplacePipe } from './pipes/code/code.replace.pipe';
import { TitleReplacePipe } from './pipes/title/title.replace.pipe';

@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptFormsModule,
        NativeScriptHttpModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forRoot(routes)
    ],
    
    declarations: [ 
        AppComponent,
        DashboardComponent, 
        CategoryComponent,
        MemoComponent,
        navigatableComponents,
        CodeReplacePipe,
        TitleReplacePipe
    ],

    bootstrap: [ 
        AppComponent 
    ]

})
export class AppModule { }

