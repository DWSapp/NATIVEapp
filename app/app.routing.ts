import { DashboardComponent } from "./components/dashboard/dashboard.cmp";
import { CategoryComponent } from "./components/category/category.cmp";
import { MemoComponent } from "./components/memo/memo.cmp";

export const routes = [
  { path: "", component: DashboardComponent },
  { path: "category", component: CategoryComponent },
  { path: "wordpress/:memoName", component: MemoComponent },
];

export const navigatableComponents = [
  DashboardComponent
];